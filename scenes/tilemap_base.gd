extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	balance()
	pass # Replace with function body.

export var timer_max = 5
var timer = 10

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if timer == 0:
		check()
		balance()
	timer -= 1;
	pass

export var max_visited_nodes_before_assumed_stable = 10000;
export var magic_tile = 1;

export var chunk: PackedScene;
onready var point_mass: PackedScene = load("res://scenes/point-mass.tscn")

func check():
	var ever_visited = PoolVector2Array([])
	var tilemap = $"../TileMap"
	var cells = tilemap.get_used_cells()
	for start in cells:
		if start in ever_visited:
			continue;
		var floating = true;
		var visited = PoolVector2Array([start]);
		var visit_queue = get_neighbors(start);
		while len(visit_queue) > 0:
			var pos = len(visit_queue) - 1
			var cell = visit_queue[pos];
			visit_queue.remove(pos);
			if not cell in cells:
				continue;
			if cell in visited:
				continue
			if cell in ever_visited and tilemap.get_cellv(cell) != -1:
				floating = false
				continue
			if tilemap.get_cellv(cell) == magic_tile:
				floating = false;
			visited.push_back(cell);
			visit_queue.append_array(get_neighbors(cell))
			if len(visited) > max_visited_nodes_before_assumed_stable:
				floating = false;
				break;
		ever_visited.append_array(visited)
		cells = tilemap.get_used_cells()
		if len(visited) < len(cells) and floating:
			var new: RigidBody2D = chunk.instance();
			new.linear_velocity = get_parent().linear_velocity
			new.angular_velocity = get_parent().angular_velocity
			new.get_node("tilemap_chunker").chunk = chunk
			new.global_transform.origin = get_parent().global_transform.xform(tilemap.map_to_world(start))
			var offset = Vector2.ZERO
			var new_tilemap: TileMap = new.get_node("TileMap")
			for i in visited:
				var pos = i - start
				offset += new_tilemap.map_to_world(pos + Vector2(0,0))
				new_tilemap.set_cellv(pos, tilemap.get_cellv(i))
				tilemap.set_cellv(i, -1);
			offset /= len(visited)
			new_tilemap.update_bitmask_region()
			#new.global_transform.origin += offset
			#new_tilemap.global_transform.origin -= offset
			new.rotation = get_parent().rotation
			get_parent().get_parent().add_child(new)

func get_neighbors(src: Vector2):
	return PoolVector2Array([
		src + Vector2.DOWN,
		src + Vector2.UP,
		src + Vector2.LEFT,
		src + Vector2.RIGHT
	])

func write(position: Vector2, tile: int):
	timer = timer_max
	var tilemap = $"../TileMap"
	tilemap.set_cellv(position, tile)
	tilemap.update_bitmask_region(position-Vector2(1,1), position+Vector2(1,1))

func balance():
	var tilemap: TileMap = $"../TileMap"
	for child in tilemap.get_children():
		child.remove_and_skip()
	var cells = tilemap.get_used_cells();
	var offset = Vector2.ZERO
	for cell in cells:
		offset += cell
	var tmp_offset = offset / len(cells)
	offset += tmp_offset
	offset /= len(cells) + 1
	offset += Vector2(0.5,0.5)
	offset *= 16
	var rigidbody = point_mass.instance()
	rigidbody.mass = len(cells)
	rigidbody.linear_velocity = get_parent().linear_velocity
	#rigidbody.angular_velocity = get_parent().angular_velocity
	#rigidbody.transform.origin = offset
	var joint: PinJoint2D = PinJoint2D.new();
	joint.transform.origin = offset
	joint.add_child(rigidbody)
	tilemap.add_child(joint)
	joint.set_node_b(joint.get_path_to(get_parent()))
	joint.set_node_a(joint.get_path_to(rigidbody))
		#var position = tilemap.map_to_world(cell)
		#var rigidbody = point_mass.instance()
		#rigidbody.transform.origin = position
		#tilemap.add_child(rigidbody)
	
