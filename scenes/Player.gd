extends RigidBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var maximum_velocity = 100;
export var force = 2000
export var jump_force = 500;
export var throw_speed = 200;
export var buildable_tilemap_path: NodePath;

onready var default_friction = physics_material_override.friction;
onready var projectile: PackedScene = load("res://scenes/miner_projectile.tscn")
onready var buildable_tilemap: RigidBody = get_node(buildable_tilemap_path)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	applied_force = Vector2.ZERO
	physics_material_override.friction = default_friction
	if Input.is_action_pressed("ui_right") and linear_velocity.x < maximum_velocity:
		applied_force.x += force
		physics_material_override.friction = 0
	if Input.is_action_pressed("ui_left") and linear_velocity.x > -maximum_velocity:
		applied_force.x -= force
		physics_material_override.friction = 0
	if Input.is_action_just_pressed("ui_accept") and $RayCast2D.is_colliding():
		applied_force.y = jump_force;
	if Input.is_action_just_pressed("fire"):
		var pos = get_global_mouse_position()
		var new = projectile.instance()
		new.global_transform.origin = pos
		get_parent().add_child(new)
	if Input.is_action_pressed("place"):
		var pos = get_global_mouse_position()
		var relative_pos = buildable_tilemap.global_transform.xform_inv(pos)
		var tile_pos = buildable_tilemap.get_node("TileMap").world_to_map(relative_pos)
		buildable_tilemap.get_node("tilemap_chunker").write(tile_pos, 0)
	
	pass
