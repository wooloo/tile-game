extends RigidBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	for body in get_colliding_bodies():
		var tilemap = body.get_node_or_null("TileMap")
		if tilemap != null:
			var our_position = global_transform.origin
			var best_distance = 10000000
			var best_tile = null
			for tile in tilemap.get_used_cells():
				var its_position = tilemap.global_transform.xform(tilemap.map_to_world(tile))
				var distance = our_position.distance_to(its_position)
				if distance <= best_distance:
					best_distance = distance
					best_tile = tile
			if best_tile != null:
				body.get_node("tilemap_chunker").write(best_tile, -1)
				for i in get_children():
					i.remove_and_skip()
				remove_and_skip()
